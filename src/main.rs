use std::time::Instant;
fn main() {
    /*let now = Instant::now();
    static BENCH: &[&[u8]] = &[
        b"124351141512412435325",
        b"Write a fast-growing assembly function",
        b"And I'm now heading to the elevator",
        b"I'm in the now infamous elevator",
        b"1549827632458914598012734",
        b"i was always mediocre at golfing",
        b"tho, i like some code challenges",
        b"I am once again in an elevator",
        b"A person with mannerism of cat",
    ];
    for arr in BENCH {
        println!("{}", unsafe { swap_count(arr) });
    }
    println!("{:?}", now.elapsed())*/
    let bytes = &mut [49, 50, 51, 52, 53, 49, 50, 51, 52, 53];
    unsafe { swap(bytes) };
    use std::io::Write;
    let _ = std::io::stdout().write_all(bytes);
    println!("{}", swap_count_map(b"1234512345"))
}

#[inline]
/// bytes.len() > 2
pub unsafe fn swap(bytes: &mut [u8]) {
    for i in 0..bytes.len() {
        let &mut first = bytes.get_unchecked_mut(i);
        if let Some(pair) = bytes
            .get_unchecked_mut(i + 1..)
            .iter()
            .position(|n| *n == first)
        {
            bytes.get_unchecked_mut(i + 1..=pair + i).reverse();
        }
    }
}
/// arr.len() > 2
#[inline]
pub unsafe fn swap_count(arr: &[u8]) -> usize {
    let mut i = 1;
    let mut vec = arr.to_owned();
    swap(&mut vec);
    while vec != arr {
        i += 1;
        swap(&mut vec);
    }
    i
}
use std::collections::{BTreeMap, BTreeSet};

type Map = BTreeMap<u8, (Vec<usize>, Vec<usize>)>;
pub fn swap_count_map(arr: &[u8]) -> usize {
    let mut count = 1;
    let mut map: Map = BTreeMap::new();
    let mut vec = arr.to_vec();
    for (i, c) in arr.iter().enumerate() {
        if map.contains_key(c) {
            let entry = map.get_mut(c).unwrap();
            entry.0.push(i);
        } else {
            map.insert(*c, (vec![i], vec![]));
        }
    }
    println!("{:?}", map);
    swap_map(&mut vec, &mut map);
    use std::io::Write;
    let _ = std::io::stdout().write_all(&vec);
    while vec != arr {
        println!("{:?}", map);
        swap_map(&mut vec, &mut map);
        count += 1;
    }
    count
}

pub fn swap_map(arr: &mut [u8], map: &mut Map) {
    let mut vec = Vec::with_capacity(arr.len());
    let mut ptr = 0;
    let mut dir = true;
    let mut visited = (0..arr.len()).collect::<BTreeSet<_>>();
    for i in 0..arr.len() {
        let c = arr.get(ptr).unwrap();
        vec.push(*c);
        let (front, back) = map.get_mut(c).unwrap();
        back.push(i);
        assert!(visited.remove(&ptr));
        if dir {
            let pos = front.binary_search(&ptr).unwrap();
            if let Some(next) = front.get(pos + 1) {
                ptr = next - 1;
                dir = false
            } else {
                if ptr == arr.len() - 1 {
                    ptr = *visited.iter().rev().next().unwrap();
                    dir = false;
                } else {
                    ptr += 1;
                }
            }
        } else {
            let pos = front.binary_search(&ptr).unwrap();
            if let Some(next) = pos.checked_sub(1).and_then(|n| front.get(n)) {
                ptr = next + 1;
                dir = true;
            } else {
                if ptr == arr.len() - 1 {
                    ptr = *visited.iter().next().unwrap();
                    dir = true;
                } else {
                    ptr -= 1;
                }
            }
        }
    }
    for i in map.iter_mut() {
        i.1 .0.clear();
        std::mem::swap(&mut i.1 .0, &mut i.1 .1);
    }
    arr.copy_from_slice(&mut vec);
}

#[cfg(test)]
mod test {
    static TESTS: &[&[u8]] = &[
        b"1231",
        b"1234512345",
        b"Sandbox for Proposed Challenges",
        b"Collatz, Sort, Repeat",
        b"Rob the King: Hexagonal Mazes",
        b"Decompress a Sparse Matrix",
        b"14126453467324231",
        b"12435114151241",
        b"Closest Binary Fraction",
        b"Quote a rational number",
        b"Solve the Alien Probe puzzle",
    ];
    static RESULT: &[usize] = &[2, 6, 1090, 51, 144, 8610, 1412, 288, 242, 282, 5598];
    #[test]
    fn test_all() {
        for (&arr, &res) in TESTS.iter().zip(RESULT) {
            assert_eq!(res, crate::swap_count_map(arr));
        }
    }
}
